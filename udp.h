#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <stdlib.h>
#include <stdint.h>
#include <inttypes.h>
#include <pthread.h>
#include <arpa/inet.h>
#include<sys/socket.h>
#include <pcap.h>
#include <string.h>
#include <sys/queue.h>
#include <stdarg.h>
#include <errno.h>
#include <getopt.h>

#include <netinet/in.h>
#include <linux/if.h>
#include <linux/if_tun.h>
#include <fcntl.h>
#include <sys/ioctl.h>

#include <rte_common.h>
#include <rte_log.h>
// 
#include <rte_memory.h>
#include <rte_memcpy.h>
#include <rte_eal.h>

#include <rte_launch.h>
#include <rte_atomic.h>
#include <rte_lcore.h>
#include <rte_branch_prediction.h>
#include <rte_interrupts.h>
#include <rte_bus_pci.h>
#include <rte_debug.h>
#include <rte_ether.h>
#include <rte_ethdev.h>
#include <rte_mempool.h>
#include <rte_mbuf.h>
#include <rte_string_fns.h>
#include <rte_cycles.h>
#include <rte_malloc.h>
#include <rte_kni.h>

#include <rte_per_lcore.h>
#include <rte_ip.h>
#include <rte_udp.h>

/* Internet address. */
struct Qin_addr {
        uint32_t       s_addr;     /* address in network byte order */
};

struct Qsockaddr_in {
	short   sin_family;
	u_short sin_port;
	struct  Qin_addr sin_addr;
	char    sin_zero[8];
};

struct pseudo_hdr
{
	uint32_t source_address;
	uint32_t dest_address;
	uint8_t placeholder;
	uint8_t protocol;
	uint16_t udp_length;
};

/**
 * Ethernet header: Contains the destination address, source address
 * and frame type.
 */
struct ether_hdr {
	u_char	ether_dhost[6]; /**< Destination address. */
	u_char	ether_shost[6]; /**< Source address. */
	uint16_t ether_type; /**< Frame type. */
};

/**
 * IPv4 Header
 */
struct ip_hdr {
	uint8_t  ihl:4;		/**< version */
	uint8_t version:4;	/**< header length */
	uint8_t  tos;	/**< type of service */
	uint16_t tot_len;		/**< length of packet */
	uint16_t id;		/**< packet ID */
	uint16_t frag_off;	/**< fragmentation offset */
	uint8_t  ttl;		/**< time to live */
	uint8_t  protocol;		/**< protocol ID */
	uint16_t check;		/**< header checksum */
	uint32_t saddr;		/**< source address */
	uint32_t daddr;		/**< destination address */
};

/**
 * UDP Header
 */
struct udp_hdr {
	uint16_t src_port;    /**< UDP source port. */
	uint16_t dst_port;    /**< UDP destination port. */
	uint16_t dgram_len;   /**< UDP datagram length */
	uint16_t dgram_cksum; /**< UDP datagram checksum */
};

/* Structure type for recording kni interface specific stats */
struct kni_interface_stats {
	uint64_t rx_packets; /* number of pkts received from NIC, and sent to KNI */
	uint64_t rx_dropped; /* number of pkts received from NIC, but failed to send to KNI */
	uint64_t tx_packets; /* number of pkts received from KNI, and sent to NIC */
	uint64_t tx_dropped; /* number of pkts received from KNI, but failed to send to NIC */
};


struct ip_arg {
	char *s_ip;
	char *d_ip;
	char *s_port;
	char *d_port;
};