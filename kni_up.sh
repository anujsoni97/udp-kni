#!/bin/sh
PORT=$1
for START in $(seq 1 $2)
do
    sudo ip link set up vEth0_$((START-1))
    sudo ifconfig vEth$((PORT))_$((START-1)) 192.168.10.5$((START-1)) 
    sudo echo 1 > /sys/devices/virtual/net/vEth$((PORT))_$((START-1))/carrier
done
