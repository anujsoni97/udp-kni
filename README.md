# UDP_KNI Application Documentation

## Description
The udp_kni applicaton, currently only runnable on 1Userver with DPDK binded inbuilt Intel 10G card (eno2/19:00.1), creates upto 32 KNI virtual Interfaces.
These interface(s) forward data from ethernet port to kni interfaces via their specific `kni_tx_burst` & `kni_rx_burst` functions. 

The application has 4 main functions (Running simultaneously on different lcores): `kni_ingress`, `kni_egress`, `rx_func`, `tx_func`.

- <strong>kni_ingress</strong>: Runs for all kni devices, `rx_eth_burst` runs on all kni devices for receiving packets.Kni_ingress, then applies a filtering logic on the packets. The logic filters on the basis of source IP address of incoming packets. For IP "192.168.10.100", The application shows Destination MAC address, Source MAC address data stored in the packet. For IP "192.168.10.101", the application calls the function `kni_tx_burst` to tx the packet to kni interface.

- <strong>kni_egress</strong>: Runs `kni_rx_burst` to get packets from kni devices and forwards them to eth port by calling `tx_burst`. Runs for all Kni devices.

- <strong>rx_func</strong>: Hardcoded to run on lcore 6. Currently dormant, as the logic is now being used after filtering in kni_ingress.

- <strong>tx_func</strong>: Hardcoded to run on lcore 7. Creates UDP packets and tx_bursts them on TxQueue no. 2.

---
## Working
![Kernel Network Interface overview](https://doc.dpdk.org/guides/_images/kernel_nic_intf.png)

The UDP KNI application is a simple example that demonstrates the use of the DPDK to create a path for packets to go through the Linux kernel and having another path for packets to go through the ethernet port bypassing the Kernel Network Interface device. This is done by filtering incoming packets & using a separate tx queue for transmitting UDP packets. 

The function `kni_loop` launches on all available lcores and subsequently runs `kni_ingress`, `kni_egress`, `rx_func`, `tx_func` on their assigned lcores.

Refer to the flowchart 1 for packetflow.

---
## Usage
For running the Application, run this example code:

```shell
sudo build/app/udp-kni -l 4-7 -n 2 -- -P -m -p 0x1 --config="(0,4,5,8)"
```
```shell
./kni_up.sh X
```
X = number of KNI devices to UP

Command line arguments information:

- `-l` Assignes lcores to with specified lcore ID to EAL. currently, the hardcoded lcores assigned to rx_func & tx_func is 6 & 7 respectively.

- `-n` Set the number of memory channels to use. Use 2 for default.

- `-P` Optional flag to set all ports to promiscuous mode so that packets are accepted regardless of the packet’s Ethernet MAC destination address. Without this option, only packets with the Ethernet MAC destination address set to the Ethernet address of the port are accepted.

- `-m` Optional flag to enable monitoring and updating of the Ethernet carrier state. With this option set, a thread will be started which will periodically check the Ethernet link status of the physical Ethernet ports and set the carrier state of the corresponding KNI network interface to match it. This means that the KNI interface will be disabled automatically when the Ethernet link goes down and enabled when the Ethernet link goes up.

- `-p` Hexadecimal bitmask of ports to configure.

- `--config="(port,lcore_rx,lcore_tx[,lcore_kthread,...])[,(port,lcore_rx,lcore_tx[,lcore_kthread,...])]"` Determines which lcores the Rx and Tx DPDK tasks, and (optionally) the KNI kernel thread(s) are bound to for each physical port.


---
## Dependencies
DPDK version 19.11
Ethernet ports binded to DPDK using vfio-pci driver


---
## TODO
1. Trying Atomic Counter variables for high performace consistent filtering.
2. Testing for running tx_func & rx_func simultaneously. **DONE**