To run the script, 2 command line arguments are required.
First is the DPDK port number, for ex, 0, 1 or 2.
Second is the number of Virtual Interfaces created for KNI.

Example command:
./kni_up 0 5
This command should up all the KNI interfaces also assigning them IP addresses.